Olá!
================

Estamos resolvendo a primeira sprint do FW neste momento(percebam que este documento está atrelado ao tempo em que foi escrito, logo ele será mudado com o tempo).

### Os nossos pontos de extensão são:
> * Avaliar objetos/produtos de forma diferente.
> * Recomendar objetos/produtos de forma diferente.

## Como planejamos fazer isso?

### Avaliação
__________________________________________________________________
> * Jogos são avaliados por aspectos(estória, jogabilidade, gráficos, duração, direção artística), que podem ser pontuados de 0 a 5, e a sua nota é obtida pela média dos aspectos.
> * Filmes recebem uma nota geral, de 0 a 5(avaliação trivial).
> * Séries recebem notas por episódio(que recebem notas gerais de 0 a 5), a nota da série é calculada através da média dos seus episódios.

### Recomendação

_________________________________________________________________
Funcionamento padrão do algoritmo(que não varia).

1. Fazemos uma lista de recomendação ingênua buscando todos os produtos/objeto consumidos/jogados/assistidos pelos amigos(e ainda não consumido/jogado/assistido pelo sujeito que receberá a recomendação, claro).

2. Pegamos essa lista e FILTRAMOS as recomendações relevantes conforme o nosso critério(que é um ponto flexível).
_________________________________________________________________


> * Jogos são recomendados através da análise dos seu dois gêneros, de estória(só eu gosto de usar essa palavra?) e de jogabilidade(um jogo pode ter uma trama de "Terror" e ter uma jogabilidade "Shooter"), nós podemos recomendar o jogo tanto pelo seu gênero(de estória) ou pela sua jogabilidade(sendo que a recomendação ideal, que nem sempre é possível, acontece quando conseguimos dar match em ambos os critérios).
> * Filmes em sua maioria tem um gênero predominante, podemos fazer uma recomendação simples com ele.
> * Podemos recomendar séries por gênero e por quantidade de episódios(é um dado relevante).

Outros critérios que podem ser adotados para priorizar recomendações(critério de desempate) é a nota do objeto/produto.


Para a primeira sprint precisamos dos:

1. Pontos fixos da rede social.

2. Instância da GSN finalizada(com os seus pontos flexíveis, obviamente).

# **Prazo: Dia 31/05/2017**

* Chat feito(problemático).
* Seguir post em uma página do FB.
* Login com BD feito.
* Avaliação jogo(flexível).