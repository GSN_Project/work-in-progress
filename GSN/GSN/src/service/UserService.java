package service;

import java.time.LocalDate;

import data.IStorage;
import exception.UsernameAlreadyExistsException;
import exception.AccountNotFoundException;
import exception.PublisherNameAlreadyExistsException;
import exception.WrongPasswordException;
import model.Publisher;
import model.SystemAgent;
import model.User;

public class UserService {
	private IStorage<User> userStorage;
	private IStorage<Publisher> publisherStorage;

	public UserService(IStorage<User> userStorage, IStorage<Publisher> publisherStorage) {
		super();
		this.userStorage = userStorage;
		this.publisherStorage = publisherStorage;
	}

	public void createUserAccount(String name, String userName, String email, String password)
								  throws UsernameAlreadyExistsException {
		if (userStorage.search(userName) == null && publisherStorage.
				search(userName) == null){
			User newUser = new User(name, userName, email, password, LocalDate.now());
			userStorage.insert(newUser);
		}
		else {
			throw new UsernameAlreadyExistsException();
		}
	}
	
	public void createPublisherAccount(String name, String email, String password)  throws PublisherNameAlreadyExistsException {
		if (publisherStorage.search(name) == null && 
				userStorage.search(name) == null) {
			Publisher newPublisher = new Publisher(name, email, password, LocalDate.now());
			publisherStorage.insert(newPublisher);
		}
		else {
			throw new PublisherNameAlreadyExistsException();
		}
	}
	
	public boolean login(String keyName, String password)  throws AccountNotFoundException, WrongPasswordException {
		SystemAgent agent = userStorage.search(keyName);
		if (agent != null && agent.getPassword().equals(password)) {
			return true;
		}
		else if (agent == null) {
			agent = publisherStorage.search(keyName);
			if(agent != null && agent.getPassword().equals(password)) return true;
			else if(agent == null)  throw new AccountNotFoundException();
		}
		else throw new WrongPasswordException();
		
		return false;
	}

	public boolean existAccount(String keyName){
		if(publisherStorage.search(keyName) == null
				&& userStorage.search(keyName) == null) return false;
		else return true;
	}
}
