package service;

import model.User;

public class ManageFriend {
	public ManageFriend() { /* Empty */ }
	
	public void add(User current, User added) {
		if (current.getFriends().get(added.getUserName()) == null) {
			current.getFriends().put(added.getUserName(), added);
			added.getFriends().put(current.getUserName(), current);
		}
	}
	
	public void remove(User current, User removed) {
		current.getFriends().remove(removed.getUserName());
		removed.getFriends().remove(current.getUserName());
	}
}
