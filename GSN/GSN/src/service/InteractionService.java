
package service;

import java.awt.Image;
import java.time.LocalDate;
import model.UserInteractions;
import model.Commentary;
import model.Interaction;
import model.Notification;
import model.Post;
import model.SystemAgent;

//Se estivessemos em C++ essa classe teria muito uso de functors.

public class InteractionService {
	
	private SystemAgent actualUser;
	
	public InteractionService(SystemAgent actualUser) {
		this.actualUser = actualUser;	
	}
	
	public void post(String content) {
		this.actualUser.getPosts().add(sendPostToUsers(new Post(this.actualUser,
													content, LocalDate.now())));
	}

	public void post(String content, Image image) {
		this.actualUser.getPosts().add(sendPostToUsers(new Post
			(this.actualUser, content, LocalDate.now(), image)));
	}

	public void reactToPost(Post post, Interaction reaction){
		post.getUserInteractions().add(new UserInteractions(this.actualUser, reaction));
		notifyUsers(post.getCreator().getName() + " reagiu ao seu post.", post.getCreator());
	}

	public void commentOnPost(Post post, String content){
		post.getComments().add(new Commentary(this.actualUser, content, LocalDate.now()));
		notifyUsers(post.getCreator().getName() + " comentou no seu post.", post.getCreator());
	}
	
	public void reactToCommentary(Commentary comment, Interaction reaction){
		comment.getUserInteractions().add(new UserInteractions(this.actualUser, reaction));
		notifyUsers(comment.getCreator().getName() + " reagiu ao seu comentário.", comment.getCreator());
	}
	
	public Post sendPostToUsers(Post post){
		new Thread(){
			@Override
			public void run(){
				for(String friend : post.getCreator().getRelatedUsers().keySet())
					post.getCreator().getRelatedUsers().get(friend).getFriendPosts().push(post);
			}
		}.start();
		return post;
	}
	
	public void notifyUsers(String information, SystemAgent actualUser){
		new Thread(){
			@Override
			public void run(){
				for(String friend : actualUser.getRelatedUsers().keySet())
					actualUser.getRelatedUsers().get(friend).getNotifications().push
								(new Notification(information, actualUser, LocalDate.now()));
			}
		}.start();
	}
}