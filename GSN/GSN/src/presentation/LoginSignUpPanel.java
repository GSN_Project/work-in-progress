package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
//import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import exception.AccountNotFoundException;
import exception.PublisherNameAlreadyExistsException;
import exception.UsernameAlreadyExistsException;
import exception.WrongPasswordException;

public class LoginSignUpPanel extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private MainFrame myFrame;

	JLabel lNotRegistered = new JLabel("Account not registred!");
	JLabel INameError = new JLabel("Name field is empty!");
	JLabel IUsernameError = new JLabel("User field is empty!");
	JLabel lPassError = new JLabel("Password field is empty!");
	JLabel lEmailError = new JLabel("Email field is empty!");
	JLabel lSignUp = new JLabel("Sign Up");
	JLabel lMessage = new JLabel("Enter with an account:");

	JTextField tName = new JTextField();
	JTextField tUsername = new JTextField();
	JTextField tEmail = new JTextField();
	JPasswordField tPassword = new JPasswordField();

	JButton bLogIn = new JButton("Log In");
	JButton bSignUp = new JButton("Sign Up");

	JCheckBox checkPublisher = new JCheckBox("Publisher");

	Font font;

	public LoginSignUpPanel(MainFrame myFrame, Font font) {
		this.font = font;
		this.myFrame = myFrame;

		this.setLayout(null);
		this.setPreferredSize(new Dimension(260, 200));
		this.setBackground(new Color(240, 240, 240));

		// Labels
		JLabel lName = new JLabel("Name:");
		JLabel lUsername = new JLabel("Username:");
		JLabel lPass = new JLabel("Password:");
		JLabel lEmail = new JLabel("Email:");

		lUsername.setFont(font);
		lPass.setFont(font);

		bLogIn.setFont(font);
		bSignUp.setFont(font);
		lNotRegistered.setFont(font);
		IUsernameError.setFont(font);
		lPassError.setFont(font);
		lSignUp.setFont(font);
		lMessage.setFont(font);
		checkPublisher.setFont(font);

		lNotRegistered.setForeground(Color.RED);
		IUsernameError.setForeground(Color.RED);
		lPassError.setForeground(Color.RED);
		lSignUp.setForeground(Color.BLUE);

		lNotRegistered.setVisible(false);
		IUsernameError.setVisible(false);
		lPassError.setVisible(false);
		checkPublisher.setVisible(false);
		bSignUp.setVisible(false);

		bLogIn.addActionListener(this);
		bSignUp.addActionListener(this);

		lName.setBounds(20, 20, 65, 20);
		lUsername.setBounds(20, 60, 65, 20);
		lPass.setBounds(20, 80, 65, 20);
		lEmail.setBounds(20, 40, 65, 20);

		tName.setBounds(90, 20, 140, 20);
		tUsername.setBounds(90, 60, 140, 20);
		tPassword.setBounds(90, 80, 140, 20);
		tEmail.setBounds(90, 40, 140, 20);
		bLogIn.setBounds(155, 125, 75, 20);
		bSignUp.setBounds(155, 125, 75, 20);
		lNotRegistered.setBounds(90, 30, 140, 20);
		IUsernameError.setBounds(90, 30, 120, 20);
		lPassError.setBounds(90, 70, 140, 20);
		lSignUp.setBounds(205, 0, 60, 20);
		lMessage.setBounds(20, 0, 130, 20);
		checkPublisher.setBounds(15, 126, 140, 20);

		lSignUp.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				lSignUp.setForeground(Color.BLUE);
			}

			@Override
			public void mousePressed(MouseEvent e) {
				lSignUp.setForeground(new Color(100, 100, 255));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				/* Empty */ }

			@Override
			public void mouseEntered(MouseEvent e) {
				/* Empty */ }

			@Override
			public void mouseClicked(MouseEvent e) {
				if (lSignUp.getText() == "Sign Up") {
					lSignUp.setText("Log in");
					lMessage.setText("Create a new account:");
					checkPublisher.setVisible(true);
					tName.setVisible(true);
					lName.setVisible(true);
					tEmail.setVisible(true);
					lEmail.setVisible(true);
					bLogIn.setVisible(false);
					bSignUp.setVisible(true);
				} else {
					lSignUp.setText("Sign Up");
					lMessage.setText("Enter with a account:");
					tName.setVisible(false);
					lName.setVisible(false);
					tEmail.setVisible(false);
					lEmail.setVisible(false);
					checkPublisher.setVisible(false);
					bSignUp.setVisible(false);
					bLogIn.setVisible(true);
				}
			}
		});

		tEmail.setVisible(false);
		lEmail.setVisible(false);
		tName.setVisible(false);
		lName.setVisible(false);

		this.add(lName);
		this.add(tName);
		this.add(lUsername);
		this.add(lPass);
		this.add(lEmail);
		this.add(lMessage);
		this.add(tUsername);
		this.add(tPassword);
		this.add(tEmail);
		this.add(bLogIn);
		this.add(lNotRegistered);
		this.add(IUsernameError);
		this.add(lPassError);
		this.add(lSignUp);
		this.add(checkPublisher);
		this.add(bSignUp);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == bLogIn) {
			// Sets error labels to not visible
			lNotRegistered.setVisible(false);
			IUsernameError.setVisible(false);
			lPassError.setVisible(false);

			// Gets the text from the fields
			boolean emptyText = false;
			String user = tUsername.getText();
			String pass = new String(tPassword.getPassword());

			// Clears the fields
			tUsername.setText("");
			tPassword.setText("");

			// Tests if the user field was empty
			if (user.length() == 0) {
				emptyText = true;
				IUsernameError.setVisible(true);
			}

			// Tests if the password field was empty
			if (pass.length() == 0) {
				emptyText = true;
				lPassError.setVisible(true);
			}

			// If any of the fields were empty, return
			if (emptyText)
				return;
			/*
			 Tries to login
			if (myFrame.getUserService().login(user, pass)) {
				this.setVisible(false);
				myFrame.changeToTimelinePanel(user);
			} else
				lNotRegistered.setVisible(true);
			*/
			try{
				myFrame.getUserService().login(user, pass);
				this.setVisible(false);
				myFrame.changeToTimelinePanel(user);
			}
			catch(AccountNotFoundException error){
				lNotRegistered.setVisible(true);
			}
			catch(WrongPasswordException error){
				lNotRegistered.setVisible(true);//Falta fazer uma nova notifica��o
			}
		} else if (e.getSource() == bSignUp) {
			// Sets error labels to not visible
			lNotRegistered.setVisible(false);
			IUsernameError.setVisible(false);
			lPassError.setVisible(false);

			// Gets the text from the fields
			boolean emptyText = false;
			String name = tName.getText();
			String user = tUsername.getText();
			String email = tEmail.getText();
			String pass = new String(tPassword.getPassword());

			// Clears the fields
			tName.setText("");
			tUsername.setText("");
			tEmail.setText("");
			tPassword.setText("");

			// Tests if the user field was empty
			if (name.length() == 0) {
				emptyText = true;
				INameError.setVisible(true);
			}

			if (user.length() == 0) {
				emptyText = true;
				IUsernameError.setVisible(true);
			}

			// Tests if the password field was empty
			if (pass.length() == 0) {
				emptyText = true;
				lPassError.setVisible(true);
			}

			if (email.length() == 0) {
				emptyText = true;
				lEmailError.setVisible(true);
			}

			// If any of the fields were empty, return
			if (emptyText)
				return;

			// Tries to logon
			//if (!myFrame.getUserService().existAccount(name) && !myFrame.getUserService().existAccount(user)) {
				if (checkPublisher.isSelected()) {
					try {
						myFrame.getUserService().createPublisherAccount(name, email, pass);
						JOptionPane.showMessageDialog(null, "Publisher registered successfully", "Success",
								JOptionPane.INFORMATION_MESSAGE);
					} catch (PublisherNameAlreadyExistsException error) {
						lNotRegistered.setVisible(true);
					}
				} else {
					try {
						myFrame.getUserService().createUserAccount(name, user, email, pass);
						JOptionPane.showMessageDialog(null, "User registered successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
					} catch (UsernameAlreadyExistsException error) {
						lNotRegistered.setVisible(true);
					}
				}
				checkPublisher.setSelected(false);
			//} else
				//lNotRegistered.setVisible(true);
		}
	}
}