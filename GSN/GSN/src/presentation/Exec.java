package presentation;

/**
 * Class responsible by executes the program.
 */
public class Exec {
	public static void main(String[] args) {
		new MainFrame();
	}
}
