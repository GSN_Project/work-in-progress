package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
//import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.Post;

public class TimelinePanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Font font;
	private WestPanel westPanel;
	private SouthPanel southPanel;
	private CenterPanel centerPanel;
	
	@SuppressWarnings("unused")
	private MainFrame superiorFrame; //Ainda n�o o usei, mas ele servir� 
									//para ter acesso aos dados do MainFrame.

	public TimelinePanel(MainFrame superiorFrame, Font font) {
		this.font = font;
		this.superiorFrame = superiorFrame;

		this.westPanel = new WestPanel();
		this.southPanel = new SouthPanel();
		this.centerPanel = new CenterPanel();

		this.setLayout(new BorderLayout());
		this.setBounds(0, 0, 800, 600);
		this.setBackground(Color.GRAY);

		this.add(westPanel, BorderLayout.WEST);
		this.add(southPanel, BorderLayout.SOUTH);
		this.add(centerPanel, BorderLayout.CENTER);
	}

	private class SouthPanel extends JPanel implements ActionListener {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public SouthPanel() {
			this.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 10));
			this.setPreferredSize(new Dimension(100, 50));
			this.setBackground(Color.LIGHT_GRAY);

			//Dimension d = new Dimension(30, 30);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			e.getSource();
		}
	}

	private class WestPanel extends JPanel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private MyScroll scroll = new MyScroll();
		private JButton addPlaylist = new JButton(new ImageIcon("icon/add.png"));
		private JButton remPlayList = new JButton(new ImageIcon("icon/rem.png"));

		public WestPanel() {
			this.setLayout(new BorderLayout());
			this.setPreferredSize(new Dimension(200, 400));

			scroll.setBorder(BorderFactory.createTitledBorder("Alguma coisa..."));

			//Dimension labelD = new Dimension(140, 30);

			Dimension buttonD = new Dimension(30, 30);
			addPlaylist.setPreferredSize(buttonD);
			remPlayList.setPreferredSize(buttonD);

			JPanel southPanel = new JPanel();
			southPanel.setLayout(new FlowLayout());
			southPanel.setPreferredSize(new Dimension(200, 80));

			/*
			 * if(superiorFrame.getController().actualIsSuper()){
			 * southPanel.add(addPlaylist); southPanel.add(addLabel);
			 * southPanel.add(remPlayList); southPanel.add(remLabel); }
			 */

			southPanel.setBackground(Color.LIGHT_GRAY);
			southPanel.setBorder(BorderFactory.createEtchedBorder());

			this.add(scroll, BorderLayout.CENTER);
			this.add(southPanel, BorderLayout.SOUTH);
		}
	}

	private class CenterPanel extends JPanel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private MyScroll scroll = new MyScroll();
		//private JButton addMusic = new JButton(new ImageIcon("icon/post.png"));
		//private JButton remMusic = new JButton(new ImageIcon("icon/rem.png"));

		public CenterPanel() {
			this.setLayout(new BorderLayout());

			scroll.setBorder(BorderFactory.createTitledBorder("P�gina Principal"));

			Dimension labelD = new Dimension(120, 30);
			JLabel addLabel = new JLabel("Postar");
			addLabel.setFont(font);
			addLabel.setPreferredSize(labelD);

			//Dimension buttonD = new Dimension(30, 30);
			
			JPanel southPanel = new JPanel();
			southPanel.setLayout(new BorderLayout());
			southPanel.setPreferredSize(new Dimension(160, 80));
			JPanel southWestPanel = new JPanel();
			southWestPanel.setPreferredSize(southPanel.getPreferredSize());
			southWestPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
			/*
			 * if(superiorFrame.getController().actualIsSuper()){
			 * southWestPanel.add(addMusic); southWestPanel.add(addLabel);
			 * southWestPanel.add(remMusic); southWestPanel.add(remLabel); }
			 */

			southWestPanel.setBackground(Color.LIGHT_GRAY);
			southPanel.add(southWestPanel, BorderLayout.WEST);
			southPanel.setBackground(Color.LIGHT_GRAY);
			southPanel.setBorder(BorderFactory.createEtchedBorder());

			this.add(scroll, BorderLayout.CENTER);
			this.add(southPanel, BorderLayout.SOUTH);
		}
	}

	public void addPostOnScroll(Post post){

		JButton button = new JButton(post.getCreator().getName());
		button.setFont(font);
		
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//Abre um frame com o post e seu conte�do(to do: classe "PostFrame").
				// O frame do post ter� um scroll de coment�rios("CommentFrame", que 
				// ser� de fato at�mico--ou seja, ele n�o vai ter um scroll para um tipo "menor" de intera��o).
			}
		});
		
		new Thread(){
			public void run(){ centerPanel.scroll.addButton(button); }
		}.start();
	}
}
