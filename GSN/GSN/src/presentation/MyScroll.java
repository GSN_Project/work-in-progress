package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

public class MyScroll extends JScrollPane {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel myPanel = new JPanel();
	private int numberOfButtons = 0;

	public MyScroll() {
		super();
		this.setPreferredSize(new Dimension(200, 400));
		JPanel aux = new JPanel();
		aux.setLayout(new BorderLayout());
		myPanel.setLayout(new GridBagLayout());
		aux.add(myPanel, BorderLayout.NORTH);
		this.setViewportView(aux);
	}

	public void addButton(JButton button) {
		if (this.contains(button))
			return;

		GridBagConstraints g = new GridBagConstraints();
		g.gridx = 0;
		g.gridy = numberOfButtons;
		g.weightx = 1.0;
		g.weighty = 0.0;
		g.fill = GridBagConstraints.HORIZONTAL;
		g.anchor = GridBagConstraints.NORTH;

		button.setBackground(Color.WHITE);
		button.setHorizontalAlignment(SwingConstants.LEFT);

		myPanel.add(button, g);
		numberOfButtons++;
	}

	public void removeButton(JButton button) {
		if (this.contains(button)) {
			myPanel.remove(button);
			numberOfButtons--;
			System.out.println("> " + numberOfButtons);
			myPanel.repaint();
		}
	}

	private boolean contains(JButton button) {
		Component[] ct = myPanel.getComponents();
		for (int i = 0;; i++) {
			try {
				if (ct[i] == button)
					return true;
			} catch (Exception e) {
				return false;
			}
		}
	}

	public void clearScroll() {
		myPanel.removeAll();
	}
}
