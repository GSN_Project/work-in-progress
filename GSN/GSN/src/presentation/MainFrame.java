package presentation;

import java.awt.BorderLayout;
//import java.awt.Button;
import java.awt.Dimension;
import java.awt.Font;
//import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import data.PublisherMemoryStorage;
import data.UserMemoryStorage;
import service.UserService;

public class MainFrame extends JFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserService accService;
	private Font font = new Font("Arial", Font.BOLD, 11);
	private LoginSignUpPanel startPanel;
	private TimelinePanel timelinePanel;
	
	JMenu menuFile = new JMenu("Conta");
	JMenuItem logOff = new JMenuItem("Log off");
	
	private JMenuBar menuBar = new JMenuBar();

	public MainFrame() {
		this.accService = new UserService(new UserMemoryStorage(), new PublisherMemoryStorage());
		
		startPanel = new LoginSignUpPanel(this, font);
		timelinePanel = new TimelinePanel(this, font);

		this.setLayout(new BorderLayout());
		this.setTitle("GSN");
		this.setMinimumSize(new Dimension(260, 190));
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		//JMenu menuFile = new JMenu("Conta");
		//JMenuItem logOff = new JMenuItem("Log off");

		menuFile.add(logOff);
		this.menuBar.add(menuFile);
		logOff.addActionListener(this);
		
		this.add(startPanel, BorderLayout.CENTER);

		// Add the playlists on scroll
		// for(Playlist e: controller.getPlaylists())
		// playerPanel.addPlaylistOnScroll(e);

		this.setVisible(true);
		this.setResizable(false);
	}

	public final UserService getUserService() {
		return this.accService;
	}

	// M�todo n�o troca de painel corretamente - Ajeitar
	public void changeToStartPanel() {
		/*this.remove(timelinePanel);
		this.remove(menuBar);
		this.setTitle("GSN");
		this.setMinimumSize(new Dimension(260, 190));
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.remove(this);
		this.add(startPanel, BorderLayout.CENTER);
		*/
	}

	public void changeToTimelinePanel(String userName) {
		this.setResizable(true);
		this.setMinimumSize(new Dimension(700, 500));
		this.setLocationRelativeTo(null);
		this.setTitle("GSN - " + userName);
		this.remove(startPanel);
		this.add(timelinePanel, BorderLayout.CENTER);
		this.add(menuBar, BorderLayout.NORTH);
	}

	public static void main(String[] args) {
		new MainFrame();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == logOff){
			new Thread(){
				@Override
				public void run(){
					changeToStartPanel();
				}
			}.start();
		}
	}
}
