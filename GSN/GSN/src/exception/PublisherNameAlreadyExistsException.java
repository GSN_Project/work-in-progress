package exception;

public class PublisherNameAlreadyExistsException extends Exception {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PublisherNameAlreadyExistsException() {}

    public PublisherNameAlreadyExistsException(String message){
    	super(message);
    }

    public PublisherNameAlreadyExistsException(Throwable cause) {
        super(cause);
    }

    public PublisherNameAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public PublisherNameAlreadyExistsException(String message, Throwable cause, boolean enableSuppression,
                                                boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
