package data;

import model.User;
import java.util.TreeMap;

public class UserMemoryStorage implements IStorage<User> {

	private TreeMap<String, User> dataBase;
	
	public UserMemoryStorage() {
		this.dataBase = new TreeMap<String, User>();
	}

	@Override
	public void insert(User user) {
		this.dataBase.put(user.getUserName(), user);
	}

	@Override
	public boolean modify(User user, User modified) {
		if(this.dataBase.replace(user.getUserName(), user, modified))
			return true;
		else 
			return false;
	}

	@Override
	public User search(String userName) {
		return this.dataBase.get(userName);
	}

	@Override
	public void remove(String userName) {
		this.dataBase.remove(userName);
	}
}