package data;

import java.util.TreeMap;

import model.Game;

public class GameMemoryStorage implements IStorage<Game> {

	private TreeMap<String, Game> dataBase;
	
	public GameMemoryStorage() {
		this.dataBase = new TreeMap<String, Game>();
	}

	@Override
	public void insert(Game game) {
		this.dataBase.put(game.getName(), game);
	}

	@Override
	public boolean modify(Game game, Game modified) {
		if(this.dataBase.replace(game.getName(), game, modified))
			return true;
		else 
			return false;
	}

	@Override
	public Game search(String name) {
		return this.dataBase.get(name);
	}

	@Override
	public void remove(String name) {
		this.dataBase.remove(name);
	}

}
