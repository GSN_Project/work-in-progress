package data;

import model.Publisher;
import java.util.TreeMap;

public class PublisherMemoryStorage implements IStorage<Publisher> {

	private TreeMap<String, Publisher> dataBase;
	
	public PublisherMemoryStorage() {
		this.dataBase = new TreeMap<String, Publisher>();
	}

	public void insert(Publisher Publisher) {
		this.dataBase.put(Publisher.getName(), Publisher);
	}

	public boolean modify(Publisher Publisher, Publisher modified) {
		if(this.dataBase.replace(Publisher.getName(), Publisher, modified))
			return true;
		else 
			return false;
	}

	public Publisher search(String publisherName) {
		return this.dataBase.get(publisherName);
	}

	public void remove(String publisherName) {
		this.dataBase.remove(publisherName);
	}
}