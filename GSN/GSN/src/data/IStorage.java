package data;

public interface IStorage<T> {
	public void insert(T object);
	public boolean modify(T actual, T modified);
	public T search(String key);
	public void remove(String key);
}