package model;

import java.time.LocalDate;

public class Notification {

	private String information;
	private SystemAgent sender;
	private LocalDate creation;
	
	public Notification(String information, SystemAgent sender, LocalDate creation) {
		super();
		this.information = information;
		this.sender = sender;
		this.creation = creation;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public SystemAgent getSender() {
		return sender;
	}

	public void setSender(SystemAgent sender) {
		this.sender = sender;
	}

	public LocalDate getCreationDate() {
		return creation;
	}

	public void setCreationDate(LocalDate creation) {
		this.creation = creation;
	}
}