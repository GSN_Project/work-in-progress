package model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Game {

	private String name;
	private String description;
	private ArrayList<Genres> hisGenres;
	private ArrayList<PlayerGames> hisPlayers;
	private LocalDate launchDay;
	private double media;
		
	public Game(String name, String description, ArrayList<Genres> hisGenres,
			ArrayList<PlayerGames> hisPlayers, LocalDate launchDay, double media) {
		super();
		this.name = name;
		this.description = description;
		this.hisGenres = hisGenres;
		this.hisPlayers = hisPlayers;
		this.launchDay = launchDay;
		this.media = media;
	}
	
	public Game(String name, String description, ArrayList<Genres> hisGenres,
			ArrayList<PlayerGames> hisPlayers, LocalDate launchDay) {
		super();
		this.name = name;
		this.description = description;
		this.hisGenres = hisGenres;
		this.hisPlayers = hisPlayers;
		this.launchDay = launchDay;
		this.media = 0.0;
	}
	
	public Game(String name, String description, ArrayList<Genres> hisGenres, LocalDate launchDay) {
		super();
		this.name = name;
		this.description = description;
		this.hisGenres = hisGenres;
		this.hisPlayers = new ArrayList<PlayerGames>();
		this.launchDay = launchDay;
		this.media = 0.0;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<Genres> getHisGenres() {
		return hisGenres;
	}

	public void setHisGenres(ArrayList<Genres> hisGenres) {
		this.hisGenres = hisGenres;
	}

	public ArrayList<PlayerGames> getHisPlayers() {
		return hisPlayers;
	}

	public void setHisPlayers(ArrayList<PlayerGames> hisPlayers) {
		this.hisPlayers = hisPlayers;
	}

	public double getMedia() {
		return media;
	}

	public void setMedia(double media) {
		this.media = media;
	}

	public void updateMedia() {
		//////////////////////// Isso vai ser substituido por observer.
		this.media = 0;
		for(PlayerGames p : hisPlayers)
			if(p.getGrade() != -1)
			media = p.getGrade()/hisPlayers.size();
	}

	public LocalDate getLaunchDay() {
		return launchDay;
	}

	public void setLaunchDay(LocalDate launchDay) {
		this.launchDay = launchDay;
	}
}