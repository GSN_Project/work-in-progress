package model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Stack;

public class User extends SystemAgent {
	
	private String userName;
	private TreeMap<String,PlayerGames> hisGames;
	private Stack<Post> friendPosts;
	private Stack<Notification> notifications;
	
	public User(String name, String userName, String email, String password, LocalDate registerLocalDate) {
		super(name, email, password, null, new ArrayList<Post>(), new TreeMap<String, User>(),
			  registerLocalDate);
		this.userName = userName;
		this.hisGames = new TreeMap<String, PlayerGames>();
		this.friendPosts = new Stack<Post>();
		this.setNotifications(new Stack<Notification>());
	}

	public User(String name, String userName, String email, String password, String description,
				TreeMap<String, PlayerGames> hisGames, TreeMap<String, User> friends,
				LocalDate registerLocalDate) {
		super(name, email, password, null, new ArrayList<Post>(), new TreeMap<String, User>(),
				 registerLocalDate);
		this.userName = userName;
		this.hisGames = hisGames;
		this.friendPosts = new Stack<Post>();
		this.setNotifications(new Stack<Notification>());
	}
	
	public User(String name, String userName, String email, String password,
				String description, TreeMap<String, PlayerGames> hisGames,
				LocalDate registerLocalDate) {
		super(name, email, password, null, new ArrayList<Post>(), new TreeMap<String, User>(),
				 registerLocalDate);
		this.userName = userName;
		this.hisGames = hisGames;
		this.friendPosts = new Stack<Post>();
		this.setNotifications(new Stack<Notification>());
	}

	public User(String name, String userName, String email, String password, String description,
			TreeMap<String, PlayerGames> hisGames, TreeMap<String, User> friends, ArrayList<Post> posts,
			Stack<Post> friendPosts, Stack<Notification> notifications, LocalDate registerLocalDate) {
		super(name, email, password, null, new ArrayList<Post>(), new TreeMap<String, User>(), registerLocalDate);
		this.userName = userName;
		this.hisGames = hisGames;
		this.friendPosts = friendPosts;
		this.notifications = notifications;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public TreeMap<String, PlayerGames> getHisGames() {
		return hisGames;
	}

	public void setHisGames(TreeMap<String, PlayerGames> hisGames) {
		this.hisGames = hisGames;
	}

	public TreeMap<String, User> getFriends() {
		return super.getRelatedUsers();
	}

	public void setFriends(TreeMap<String, User> friends) {
		super.setRelatedUsers(friends);
	}

	public Stack<Post> getFriendPosts() {
		return friendPosts;
	}

	public void setFriendPosts(Stack<Post> friendPosts) {
		this.friendPosts = friendPosts;
	}

	public Stack<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(Stack<Notification> notifications) {
		this.notifications = notifications;
	}
}