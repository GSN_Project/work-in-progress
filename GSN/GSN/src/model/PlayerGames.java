package model;


public class PlayerGames {
	
	private Game game;
	private User player;
	private String critic;
	private double grade;
	
	public PlayerGames(Game game, User player, String critic, double grade) {
		super();
		this.game = game;
		this.player = player;
		this.critic = critic;
		this.grade = grade;
		this.game.updateMedia();
	}
	
	public PlayerGames(Game game, User player) {
		super();
		this.game = game;
		this.player = player;
		this.critic = null;
		grade = -1;
	}

	public Game getGame() {
		return game;
	}
	
	public void setGame(Game game) {
		this.game = game;
	}
	public User getPlayer() {
		return player;
	}
	public void setPlayer(User player) {
		this.player = player;
	}
	public String getCritic() {
		return critic;
	}
	public void setCritic(String critic) {
		this.critic = critic;
	}

	public double getGrade() {
		return grade;
	}

	public void setGrade(double grade) {
		this.grade = grade;
	}
}