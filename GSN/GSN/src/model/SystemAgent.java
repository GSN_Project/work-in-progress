package model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.TreeMap;

public abstract class SystemAgent {

	private String name;
	private String email;
	private String password;
	private String description;
	private ArrayList<Post> posts;
	private TreeMap<String, User> relatedUsers;
	private LocalDate registerLocalDate;

	public SystemAgent(String name, String email, String password, String description, ArrayList<Post> posts,
			TreeMap<String, User> relatedUsers, LocalDate registerLocalDate) {
		super();
		this.name = name;
		this.email = email;
		this.password = password;
		this.description = description;
		this.posts = posts;
		this.relatedUsers = relatedUsers;
		this.registerLocalDate = registerLocalDate;
	}

	public ArrayList<Post> getPosts() {
		return posts;
	}

	public void setPosts(ArrayList<Post> posts) {
		this.posts = posts;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LocalDate getRegisterLocalDate() {
		return registerLocalDate;
	}
	
	public void setRegisterLocalDate(LocalDate registerLocalDate) {
		this.registerLocalDate = registerLocalDate;
	}

	public TreeMap<String, User> getRelatedUsers() {
		return relatedUsers;
	}

	public void setRelatedUsers(TreeMap<String, User> relatedUsers) {
		this.relatedUsers = relatedUsers;
	}
}