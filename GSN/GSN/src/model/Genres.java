package model;

public enum Genres {
	Action, Stealth, Fighting, Racing, 
	Platform, FPS, TPS, Shooter, Sports,
	RPG, JRPG, Survival, Metroidvania, SandBoxRPG,
	TaticalRPG, RTS, MOBA, MMO, MMORPG, Party, CardGame,
	SeriousGame,Rhythm, Music, TBS, TowerDefense, WarGame, 
	Choices, VisualNovel, Text, OpenWorld, Arcade, BeatNup,
	SideScrolling, GirlsGame;
}