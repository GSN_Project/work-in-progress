package model;

public class UserInteractions {

	private SystemAgent user;
	private Interaction interaction;
	
	public UserInteractions(SystemAgent user, Interaction interaction) {
		super();
		this.user = user;
		this.interaction = interaction;
	}

	public SystemAgent getUser() {
		return user;
	}

	public void setUser(SystemAgent user) {
		this.user = user;
	}

	public Interaction getInteraction() {
		return interaction;
	}

	public void setInteraction(Interaction interaction) {
		this.interaction = interaction;
	}
}