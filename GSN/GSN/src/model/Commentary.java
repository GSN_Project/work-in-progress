package model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Commentary {

	private String content;
	private SystemAgent creator;
	private ArrayList<UserInteractions> userInteractions;
	private LocalDate creationDate;
	private boolean isEdited;
	private LocalDate editionDate;

	public Commentary(SystemAgent creator, String content, LocalDate creationDate) {
		this.content = content;
		this.creator = creator;
		this.userInteractions = new ArrayList<UserInteractions>();
		this.creationDate = creationDate;
		this.isEdited = false;
		this.editionDate = null;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public SystemAgent getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public ArrayList<UserInteractions> getUserInteractions() {
		return userInteractions;
	}

	public void setUserInteractions(ArrayList<UserInteractions> userInteractions) {
		this.userInteractions = userInteractions;
	}

	public LocalDate getcreationDate() {
		return creationDate;
	}

	public void setcreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}

	public boolean isEdited() {
		return isEdited;
	}

	public void setEdited(boolean isEdited) {
		this.isEdited = isEdited;
	}

	public LocalDate getEditionDate() {
		return editionDate;
	}

	public void setEditionDate(LocalDate editionDate) {
		this.editionDate = editionDate;
	}
}
