package model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.TreeMap;

public class Publisher extends SystemAgent {
	
	private TreeMap<String,Game> publishedGames;

	public Publisher(String name, String password, String email,  LocalDate registerLocalDate) {
		super(name, email, password, null, new ArrayList<Post>(), new TreeMap<String, User>(),
			 registerLocalDate);
		this.publishedGames = new TreeMap<String, Game>();
	}
	
	public Publisher(String name, String password, String email,  LocalDate registerLocalDate,
			TreeMap<String, Game> publishedGames) {
		super(name, email, password, null, new ArrayList<Post>(), new TreeMap<String, User>(), registerLocalDate);
		this.publishedGames = publishedGames;
	}
	
	public Publisher(String name, String password, String email, String description, 
			ArrayList<Post> posts, TreeMap<String, User> followersOrFriends, LocalDate registerLocalDate, 
			TreeMap<String, Game> publishedGames) {
		super(name, email, password, description, posts, followersOrFriends, registerLocalDate);
		this.publishedGames = publishedGames;
	}

	public TreeMap<String,Game> getPublishedGames() {
		return publishedGames;
	}

	public void setPublishedGames(TreeMap<String,Game> publishedGames) {
		this.publishedGames = publishedGames;
	}

	public TreeMap<String,User> getFollowers() {
		return super.getRelatedUsers();
	}

	public void setFollowers(TreeMap<String, User> followers) {
		super.setRelatedUsers(followers);
	}
}
