package model;

import java.awt.Image;
import java.time.LocalDate;
import java.util.ArrayList;

public class Post {

	private String content;
	private Image image;
	private SystemAgent creator;
	private ArrayList<UserInteractions> userInteractions;
	private ArrayList<Commentary> comments;
	private LocalDate creationDate;
	private boolean isEdited;
	private LocalDate editionDate; 
	
	public Post(SystemAgent creator, String content, LocalDate creationDate, Image image) {
		this.content  = content;
		this.image = image;
		this.creator = creator;
		this.userInteractions = new ArrayList<UserInteractions>();
		this.isEdited = false;
		this.creationDate = creationDate;
		this.editionDate = null;
	}
	
	public Post(SystemAgent creator, String content, LocalDate creationDate) {
		this.content  = content;
		this.image = null;
		this.creator = creator;
		this.userInteractions = new ArrayList<UserInteractions>();
		this.isEdited = false;
		this.creationDate = creationDate;
		this.editionDate = null;
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public SystemAgent getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public ArrayList<UserInteractions> getUserInteractions() {
		return userInteractions;
	}

	public void setUserInteractions(ArrayList<UserInteractions> userInteractions) {
		this.userInteractions = userInteractions;
	}

	public ArrayList<Commentary> getComments() {
		return comments;
	}

	public void setComments(ArrayList<Commentary> comments) {
		this.comments = comments;
	}

	public LocalDate getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}

	public boolean isEdited() {
		return isEdited;
	}

	public void setEdited(boolean edited) {
		this.isEdited = edited;
	}

	public LocalDate getEditionDate() {
		return editionDate;
	}

	public void setEditionDate(LocalDate editionDate) {
		this.editionDate = editionDate;
	}
}
