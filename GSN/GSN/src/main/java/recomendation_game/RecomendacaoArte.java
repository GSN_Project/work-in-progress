package recomendation_game;

import java.util.List;

import dominio.Game;

public class RecomendacaoArte extends RecomendacaoGameStrategy{
	@Override
	public List<Game> recomendarJogo(String login, String qs) {
		
		qs = "select c from Game c where c.login = :login order by arte";
		return super.recomendarJogo(login, qs);
	}
}
