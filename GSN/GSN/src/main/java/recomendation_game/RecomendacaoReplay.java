package recomendation_game;

import java.util.List;

import dominio.Game;

public class RecomendacaoReplay extends RecomendacaoGameStrategy{
	@Override
	public List<Game> recomendarJogo(String login, String qs) {
		
		qs = "select c from Game c where c.login = :login order by replay";
		return super.recomendarJogo(login, qs);
	}
}
