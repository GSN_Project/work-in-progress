package recomendation_game;

import java.util.List;

import javax.inject.Inject;

import dao.GameDAO;
import dominio.Game;

public abstract class RecomendacaoGameStrategy {
	@Inject
	private GameDAO gameDAO;
	
	public  List<Game> recomendarJogo(String login,String qs){
		
		return gameDAO.recomendar(login,qs);
	}
}
