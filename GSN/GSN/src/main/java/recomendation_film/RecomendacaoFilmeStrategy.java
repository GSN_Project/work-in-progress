package recomendation_film;

import java.util.List;

import javax.inject.Inject;

import dao.FilmeDAO;
import dominio.Filme;

public abstract class RecomendacaoFilmeStrategy {
	@Inject
	private FilmeDAO filmeDAO;
	
	public  List<Filme> recomendarFilme(String login,String qs){
		
		return filmeDAO.recomendar(login,qs);
	}
	
}
