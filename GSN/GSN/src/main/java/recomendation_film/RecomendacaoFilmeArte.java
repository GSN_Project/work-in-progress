package recomendation_film;

import java.util.List;

import dominio.Filme;

public class RecomendacaoFilmeArte extends RecomendacaoFilmeStrategy{
	@Override
	public List<Filme> recomendarFilme(String login, String qs) {
		
		qs = "select c from Filme c where c.login = :login order by arte";
		return super.recomendarFilme(login, qs);
	}
}
