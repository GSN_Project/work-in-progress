package servlets;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet do chat
 * 
 *
 */

@SuppressWarnings("serial")
@WebServlet("/chat")
public class Msg extends HttpServlet  {
	public String str="";
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException{
		//if(  (String)request.getSession().getAttribute("userName") != null ){
		
		HttpSession session = request.getSession();
		String login = (String)session.getAttribute("login");
		
		PrintWriter out = response.getWriter();
		
		response.setContentType("text/html;charset=UTF-8");
		
		//Display Chat Room
        out.println("<html>  <head> <body bgcolor=\"#6495ED\"> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"> <title>Chat Room</title>  </head>");
        out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"> <center>");
        out.println("<h2>Hi ");
        //out.println(username);
        out.println("<br> Welcome to Hello Chat ");
        out.println("</h2><br><hr>");
        out.println("  <body>");
        out.println("      <form name=\"chat\" action=\"chat\">");
        out.println("Message: <input type=\"text\" name=\"txtMsg\" value=\"\" /><input type=\"submit\" value=\"Send\" name=\"cmdSend\"/>");
        out.println("<br><br> <a href=\"chat\">Refresh Chat Room</a>");
        out.println("<br>  <br>");
        out.println("Messages in Chat Box:");
        out.println("<br><br>");
        out.println("<textarea  readonly=\"readonly\"   name=\"txtMessage\" rows=\"20\" cols=\"60\">");
        if(request.getParameter("txtMsg")!=null){
        	
        	str = str + login+": "+request.getParameter("txtMsg")+ "\n";
        	out.println(str);
        }
        
        out.println("\n");
        out.println("</textarea>");
        out.println("<hr>");
        out.println("</form>");
        out.println("<a href=\"http://localhost:8080/projectONE/userpage.jsf\" >voltar</a>");
        out.println("</body>");
        out.println("</html>");
        
	}
}
