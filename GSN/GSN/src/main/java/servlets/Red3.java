package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet que pega qual a seleçao da tabela da pagina de usuario para adaptar a recomendacao
 * 
 *
 */

@SuppressWarnings("serial")
@WebServlet("/red3")
public class Red3 extends HttpServlet {
	@Override 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 

		HttpSession session = request.getSession();
		session.setAttribute("selector", 3);
		
		response.sendRedirect("userpage.jsf");
	}
}