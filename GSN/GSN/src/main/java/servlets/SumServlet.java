package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet que joga na sessao usuarios do League Of LeGENDS
 * 
 *
 */

@SuppressWarnings("serial")
@WebServlet("/searchsum")
public class SumServlet extends HttpServlet {
	@Override 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		String summoner = (String)request.getParameter("summoner");

		HttpSession session = request.getSession();
		//session.setAttribute("summoner", "Sereia Ensaboada");
		session.setAttribute("summoner", summoner);
		response.sendRedirect("http://localhost:8080/projectONE/second");
	}
}