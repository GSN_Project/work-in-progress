package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import apiCalls.RiotAPI;
import dominio.Usuario;

/**
 * Servlet que salva os dados de invocador do League Of Legends na sessao
 * 
 *
 */

@SuppressWarnings("serial")
@WebServlet("/second")
public class SecondServlet extends HttpServlet {
	@SuppressWarnings("unchecked")
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ServletContext context = request.getSession().getServletContext();		

		ArrayList<Usuario> users = new ArrayList<Usuario>();
		
		if (context.getAttribute("connected") != null) {
			users = (ArrayList<Usuario>) context.getAttribute("connected");
		}

		HttpSession session = request.getSession();
		
		String summoner = (String) session.getAttribute("summoner");

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html><body><h1>Summoner " +

				summoner + "</h1></body></html>");

		for (Usuario cz : users) {
			out.println("<html><body><h1>Users also connected " +

			cz.getLogin() + "</h1></body></html>");
		}
		
		
		ArrayList<String> champs = new ArrayList<String>();
		RiotAPI api = new RiotAPI();
		out.println("<html><body>");
		
		
		
		champs = api.doRequestChamps(summoner);

		System.out.println("INVOCADOR AKI "+champs.get(0));
		out.println("<table width=\"30%\" height=\"30%\" align=\"center\" valign=\"center\" >");
		out.println("<tr>");
		out.println("<td>");
		session.setAttribute("chmp1", champs.get(0));
		out.println("<h5>"+champs.get(0) +"</h5>");
		session.setAttribute("img1", "http://ddragon.leagueoflegends.com/cdn/6.24.1/img/champion/"+  champs.get(0)+".png");
		out.println("<img src=\"http://ddragon.leagueoflegends.com/cdn/6.24.1/img/champion/"+  champs.get(0)+".png\"/>");
		out.println("</td>");
		out.println("<td>");
		session.setAttribute("chmp2", champs.get(1));
		out.print("<h5>"+champs.get(1) +"</h5>");
		session.setAttribute("img2", "http://ddragon.leagueoflegends.com/cdn/6.24.1/img/champion/"+  champs.get(1)+".png");
		out.print("<img src=\"http://ddragon.leagueoflegends.com/cdn/6.24.1/img/champion/"+  champs.get(1)+".png\"/>");
		out.println("</td>");
		out.println("<td>");
		session.setAttribute("chmp3", champs.get(2));
		out.print("<h5>"+champs.get(2) +"</h5>");
		session.setAttribute("img3", "http://ddragon.leagueoflegends.com/cdn/6.24.1/img/champion/"+  champs.get(2)+".png");
		out.print("<img src=\"http://ddragon.leagueoflegends.com/cdn/6.24.1/img/champion/"+  champs.get(2)+".png\"/>");
		out.println("</td>");
		out.println("</tr>");
		out.println("</table>");
		out.print("</body></html>");
		out.println("<br>");
		String soloData = api.doRequestSolo(summoner);
		session.setAttribute("chmpdata", soloData);
		out.println( soloData );
		if( soloData.toLowerCase().contains("unranked") ){
			//out.println("<img src=\"https://www.unrankedsmurfs.com/images/upload/blog/2017/01/unranked.png\"/>");
			session.setAttribute("eloimg", "https://www.unrankedsmurfs.com/images/upload/blog/2017/01/unranked.png");
		}
		if( soloData.toLowerCase().contains("bronze") ){
			session.setAttribute("eloimg", "http://meuleague.com/lol/images/badges/bronze_i.png");
			//out.println("<img src=\"http://meuleague.com/lol/images/badges/bronze_i.png\"/>");
		}
		if( soloData.toLowerCase().contains("silver") ){
			session.setAttribute("eloimg", "http://meuleague.com/lol/images/badges/silver_i.png");
			//out.println("<img src=\"http://meuleague.com/lol/images/badges/silver_i.png\"/>");
		}
		if( soloData.toLowerCase().contains("gold") ){
			session.setAttribute("eloimg", "http://meuleague.com/lol/images/badges/gold_i.png");
			//out.println("<img src=\"http://meuleague.com/lol/images/badges/gold_i.png\"/>");
		}
		if( soloData.toLowerCase().contains("platinum") ){
			session.setAttribute("eloimg", "http://meuleague.com/lol/images/badges/platinum_i.png");
			//out.println("<img src=\"http://meuleague.com/lol/images/badges/platinum_i.png\"/>");
		}
		if( soloData.toLowerCase().contains("diamond") ){
			session.setAttribute("eloimg", "http://meuleague.com/lol/images/badges/diamond_i.png");
			//out.println("<img src=\"http://meuleague.com/lol/images/badges/diamond_i.png\"/>");
		}
		if( soloData.toLowerCase().contains("master") ){
			session.setAttribute("eloimg", "https://boosteria.org/img/divisions/master.png");
			//out.println("<img src=\"https://boosteria.org/img/divisions/master.png\"/>");
		}
		if( soloData.toLowerCase().contains("challenger") ){
			session.setAttribute("eloimg", "http://meuleague.com/lol/images/badges/challenger_i.png");
			//out.println("<img src=\"http://meuleague.com/lol/images/badges/challenger_i.png\"/>");
		}
		response.sendRedirect("http://localhost:8080/projectONE/gamerate.jsf");
		out.println("<a href=\"http://localhost:8080/projectONE/post.jsf\">Click here</a>");
		
	}
}