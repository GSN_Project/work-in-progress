package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UsuarioDAO;
import dominio.Usuario;

/**
 * 
 * Servlet para deleçao de usuarios do sistema
 *
 */


@SuppressWarnings("serial")
@WebServlet("/deletion")
public class DelServlet extends HttpServlet {
	@Override 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 

		String login = (String)request.getParameter("log");
		UsuarioDAO usuarioDAO = new UsuarioDAO();
		Usuario c = usuarioDAO.buscarLogin(login);
		usuarioDAO.remover(c);
		
		//LoginService service = new LoginService();
		
		//service.removerContato(login);
		//response.sendRedirect("http://localhost:8080/projectONE/userpage.jsf");
	}
}