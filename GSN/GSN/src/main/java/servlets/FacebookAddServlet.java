package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet para adicionar a variavel de sessao que passa paginas do facebook
 * 
 *
 */

@SuppressWarnings("serial")
@WebServlet("/add")
public class FacebookAddServlet extends HttpServlet {
	@Override 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		HttpSession session = request.getSession();
		
		int add = (Integer)session.getAttribute("fbpage");
		add = add + 5;
		
		session.setAttribute("fbpage", add);
		
		response.sendRedirect("http://localhost:8080/projectONE/fbapi");
		
	}
}