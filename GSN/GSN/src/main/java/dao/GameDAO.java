package dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import dominio.Game;


@Stateless
public class GameDAO {
	@PersistenceContext
	private EntityManager em;
	
	public void salvar(Game c) {
		em.persist(c);
	}
	
	public void atualizar(Game c) {
		em.merge(c);
	}
	
	public void remover(Game c) {
		c = em.find(Game.class, c.getName());
		em.remove(c);
	}
	
	@SuppressWarnings("unchecked")
	public List<Game> listar() {
		String qs = "select c from Game c";
		Query q = em.createQuery(qs);
		return (List<Game>) q.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Game> recomendar(String login,String qs){
		
		Query q = em.createQuery(qs);
		q.setParameter("login", login);		
		return (List<Game>)q.getResultList();
	}
	
	public Game buscar(int id) {
		String qs = "select c from Game c where c.id = :id";
		Query q = em.createQuery(qs);
		q.setParameter("id", id);
		try {
			return (Game) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
}
