package dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import dominio.Livro;

@Stateless
public class LivroDAO {
	@PersistenceContext
	private EntityManager em;
	
	public Livro buscarLivro(int id) {
		return (Livro) em.find(Livro.class, id);
	}
	
	public void salvar(Livro c) {
		em.persist(c);
	}
	
	public void atualizar(Livro c) {
		em.merge(c);
	}
	
	public void remover(Livro c) {
		c = em.find(Livro.class, c.getId());
		em.remove(c);
	}
	
	@SuppressWarnings("unchecked")
	public List<Livro> recomendar(String login,String qs){

		Query q = em.createQuery(qs);
		q.setParameter("login", login);		
		return (List<Livro>)q.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Livro> listar() {
		String qs = "select c from Livro c";
		Query q = em.createQuery(qs);
		return (List<Livro>) q.getResultList();
	}
	
}