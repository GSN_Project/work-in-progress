package dominio;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="livros")
public class Livro {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;
	
	@Column(nullable=false)
	private String name;
		
	@Column
	private String login;
	
	@Column
	private int criatividade;
	
	@Column
	private int autor;
	
	@Column
	private int ilustracoes;

	@Column
	private int escrita;
	
	@Column
	private int plot;
	
	public Livro() {
		
	}
	
	public Livro(String name) {
		this.name = name;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public int getCriatividade() {
		return criatividade;
	}

	public void setCriatividade(int criatividade) {
		this.criatividade = criatividade;
	}

	public int getAutor() {
		return autor;
	}

	public void setAutor(int autor) {
		this.autor = autor;
	}

	public int getIlustracoes() {
		return ilustracoes;
	}

	public void setIlustracoes(int ilustracoes) {
		this.ilustracoes = ilustracoes;
	}

	public int getEscrita() {
		return escrita;
	}

	public void setEscrita(int escrita) {
		this.escrita = escrita;
	}

	public int getPlot() {
		return plot;
	}

	public void setPlot(int plot) {
		this.plot = plot;
	}


	
}