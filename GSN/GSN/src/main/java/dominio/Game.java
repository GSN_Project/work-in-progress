package dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="games")
public class Game {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;
	
	@Column(nullable=false)
	private String name;
	
	@Column(nullable=false)
	private String login;	
	
	@Column
	private int jogabilidade;
	
	@Column
	private int grafico;
	
	@Column
	private int historia;

	@Column
	private int arte;
	
	@Column
	private int replay;
	
	/*@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "Usu_Game",
	        joinColumns = @JoinColumn(name = "idGame", referencedColumnName = "summoner"),
	        inverseJoinColumns = @JoinColumn(name = "idUser", referencedColumnName = "login"))
	private List<Usuario> usuarios;*/
	
	public Game() {
		
	}
	
	public Game(String name, String login) {
		this.name = name;
		this.login = login;
		
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getJogabilidade() {
		return jogabilidade;
	}

	public void setJogabilidade(int jogabilidade) {
		this.jogabilidade = jogabilidade;
	}

	public int getGrafico() {
		return grafico;
	}

	public void setGrafico(int grafico) {
		this.grafico = grafico;
	}

	public int getHistoria() {
		return historia;
	}

	public void setHistoria(int historia) {
		this.historia = historia;
	}

	public int getArte() {
		return arte;
	}

	public void setArte(int arte) {
		this.arte = arte;
	}

	public int getReplay() {
		return replay;
	}

	public void setReplay(int replay) {
		this.replay = replay;
	}

	
}