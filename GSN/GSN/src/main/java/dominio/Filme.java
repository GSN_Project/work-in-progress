package dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="filmes")
public class Filme {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;
	
	@Column(nullable=false)
	private String name;
		
	@Column
	private String login;
	
	@Column
	private int originalidade;
	
	@Column
	private int efeitos_especiais;
	
	@Column
	private int trilha_sonora;

	@Column
	private int arte;
	
	@Column
	private int plot;
	
	public Filme() {
		
	}
	
	public Filme(String name) {
		this.name = name;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOriginalidade() {
		return originalidade;
	}

	public void setOriginalidade(int originalidade) {
		this.originalidade = originalidade;
	}

	public int getEfeitos_especiais() {
		return efeitos_especiais;
	}

	public void setEfeitos_especiais(int efeitos_especiais) {
		this.efeitos_especiais = efeitos_especiais;
	}

	public int getTrilha_sonora() {
		return trilha_sonora;
	}

	public void setTrilha_sonora(int trilha_sonora) {
		this.trilha_sonora = trilha_sonora;
	}

	public int getArte() {
		return arte;
	}

	public void setArte(int arte) {
		this.arte = arte;
	}

	public int getPlot() {
		return plot;
	}

	public void setPlot(int plot) {
		this.plot = plot;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}



	
}