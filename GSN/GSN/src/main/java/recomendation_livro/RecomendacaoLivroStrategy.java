package recomendation_livro;

import java.util.List;

import javax.inject.Inject;

import dao.LivroDAO;
import dominio.Livro;

public abstract class RecomendacaoLivroStrategy {
	@Inject
	private LivroDAO livroDAO;
	
	public  List<Livro> recomendarLivro(String login,String qs){
		
		return livroDAO.recomendar(login,qs);
	}
}
