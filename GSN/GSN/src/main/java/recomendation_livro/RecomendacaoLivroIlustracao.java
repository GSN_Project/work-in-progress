package recomendation_livro;

import java.util.List;

import dominio.Livro;

public class RecomendacaoLivroIlustracao extends RecomendacaoLivroStrategy{
	@Override
	public List<Livro> recomendarLivro(String login, String qs) {
		
		qs = "select c from Livro c where c.login = :login order by ilustracoes";
		return super.recomendarLivro(login, qs);
	}
}
