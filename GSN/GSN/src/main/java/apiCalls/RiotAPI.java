package apiCalls;


import java.util.ArrayList;
import java.util.List;
/*
import com.robrua.orianna.api.dto.BaseRiotAPI;
import com.robrua.orianna.type.core.common.QueueType;
import com.robrua.orianna.type.core.common.Region;
import com.robrua.orianna.type.dto.league.League;
import com.robrua.orianna.type.dto.staticdata.Champion;
import com.robrua.orianna.type.dto.staticdata.ChampionList;
import com.robrua.orianna.type.dto.stats.AggregatedStats;
import com.robrua.orianna.type.dto.stats.ChampionStats;
import com.robrua.orianna.type.dto.summoner.Summoner;
import net.rithms.riot.api.RiotApi;
import net.rithms.riot.api.RiotApiException;*/


import net.rithms.riot.api.RiotApi;
import net.rithms.riot.api.RiotApiException;
import net.rithms.riot.constant.QueueType;
import net.rithms.riot.constant.Region;
import net.rithms.riot.constant.Season;
import net.rithms.riot.dto.Champion.Champion;
//import net.rithms.riot.dto.Static.Champion;
import net.rithms.riot.dto.Champion.ChampionList;
import net.rithms.riot.dto.League.League;
import net.rithms.riot.dto.League.LeagueEntry;
import net.rithms.riot.dto.Stats.AggregatedStats;
import net.rithms.riot.dto.Stats.ChampionStats;
import net.rithms.riot.dto.Stats.RankedStats;
import net.rithms.riot.dto.Summoner.Summoner;

/**
 *
 * @author the_p_000
 */
public class RiotAPI {

    //private RiotApi api;
    public RiotAPI() {
        //api = new RiotApi("RGAPI-70289B9C-E790-473D-9D6B-8E15BCC1CA13", Region.BR);
        //api.setSeason(Season.CURRENT);
    }
/**
 * 
 * @param name = nome do invocador ( summoner )
 * @return estatisticas sobre a fila ranqueada solo do jogador
 */
    public String doRequestSolo(String name) {
        RiotApi api = new RiotApi("RGAPI-70289B9C-E790-473D-9D6B-8E15BCC1CA13");//chave associada a minha conta de dev
        String str = " Unranked ";

        try {
            net.rithms.riot.dto.Summoner.Summoner summoner = api.getSummonerByName(net.rithms.riot.constant.Region.BR, name);//recebendo o invocador( summoner ) da api da riot
            long id = summoner.getId();//id do invocador

            List<net.rithms.riot.dto.League.League> leagues = api.getLeagueEntryBySummoner(net.rithms.riot.constant.Region.BR, id);//ligas ranqueadas do invocador ( flex, solo , normal, aram...)

            for (net.rithms.riot.dto.League.League league : leagues) { //percorrendo as filas ranqueadas
                net.rithms.riot.dto.League.LeagueEntry entry = league.getEntries().get(0); //LeagueEntry eh a classe onde tem as estatisticas das filas
                //System.out.println(league.getName());
                if (league.getQueue().equals(QueueType.RANKED_SOLO_5x5.name())) { //Pegando somente da fila ranqueada solo
                    str = new String();  //esses elementos HTML que tem aki nessa string eh para formatar depois la dentro do JFrame pois la nao tem quebra de linha com/n
                    str =  "Tier: " + league.getTier() +" \n "; //tier ( elo )
                    str = str + "Division: " + entry.getDivision() +" \n "; //divisao do tier
                    str = str + "LP: " + entry.getLeaguePoints() +" \n "; //pontos ranqueados da divisao
                    str = str + "Wins: " + entry.getWins() +" \n "; // vitorias ranqueadas solo
                    str = str + "Losses: " + entry.getLosses() ; //derrotas ranqueadas solo

                }
            }
        } catch (RiotApiException e) {
            System.out.println(e.getStackTrace());
            //JOptionPane.showMessageDialog(null, "Erro ao solicitar API");
        }

        return str; //retornando informacoes de liga
    }
/**
 * 
 * @param name = summoner name ( nome de invocador/usuario )
 * @return estatisticas sobre os campeoes mais utilizados do invocador
 */
    public ArrayList<String> doRequestChamps(String name) { //Requisitando informacoes sobre os campeos dos quais o usuario joga partidas ranqueadas solo
        RiotApi api = new RiotApi("RGAPI-70289B9C-E790-473D-9D6B-8E15BCC1CA13", Region.BR);//minha chave da api de dev
        String response= new String(); //mesma coisa explicada la pela linha 76 em str = new String();
        ArrayList<String> names = new ArrayList<>();
        try {
        	
            Summoner summoner = api.getSummonerByName(Region.BR, name); //buscando o invocador ( summoner )
            long id = summoner.getId(); //id do invocador
            System.out.println(summoner.getName() + " is a level " + summoner.getSummonerLevel() + " summoner on the BR server.");//pegando da api o level de invocador 
            //ChampionList champs = api.getChampions();
            //List<Champion> champions = new ArrayList<>(champs.getChampions());
            //System.out.println("He enjoys playing LoL on all different champions, like " + champions.get((int) (champions.size() * Math.random())) + ".");
            League challenger = api.getChallengerLeague(QueueType.RANKED_SOLO_5x5); //pegando os jogadores de liga challenger ( n tem utilidade atualmente )
            String aChallenger = challenger.getEntries().get(0).getPlayerOrTeamName();//pegando um jogador challenger ( tambem nao tem utilidade atualmente )
            System.out.println("He's much better at writing Java code than he is at LoL. He'll never be as good as " + aChallenger + ".");

            RankedStats rnksts = api.getRankedStats(id); //pegando os status de partidas ranqueadas do usuario
            List<ChampionStats> statList = rnksts.getChampions(); //atribuindo a uma lista de campeoes a ser percorrida

            ChampionStats higher = statList.get(0); // higher eh as estatisticas de campeao mais utilizado atualmente ( higher sera atualizado 3 vezes para devolver 3 champs diferentes )
            ChampionStats lower = statList.get(0); //(nao tem nenhuma utilidade particular) eh utilizado para resetar o valor de higher

            for (int i = 0; i < 3; i++) { // 3 iteracoes para atualizar higher
                for (ChampionStats championStats : statList) { //iterando sobre a lista de estatisticas
                    if (championStats.getId() != 0) {// id 0 is for overall ranked stats (= existe dentro dessa lista um elemento que eh a soma de todos os outros, isso exclui ele )
                        if (championStats.getStats().getTotalSessionsPlayed() > higher.getStats().getTotalSessionsPlayed()) {// comparando partidas jogadas
                            higher = championStats;// velho esquema de sempre, se ha um maior, esse vai ser o maior
                        }
                        if (championStats.getStats().getTotalSessionsPlayed() < lower.getStats().getTotalSessionsPlayed()) {// comparando partidas jogadas
                            lower = championStats;// * ha menor, menor...*
                        }
                    }
                }

                statList.remove(higher);// removendo da lista ( por que? ) para que quando o codigo for buscar o proximo campeao mais jogado ele nao considere um que ja foi contado

                System.out.println("higher times: " + higher.getStats().getTotalSessionsPlayed());//quantidade de vezes que um champ foi jogado em FILAS RANQUEADAS SOLO               
                net.rithms.riot.dto.Static.Champion chmp = api.getDataChampion(higher.getId());// pegando o campeao baseado nas estatisticas ranqueadas
                System.out.println("higher champ: " + chmp.getName()); 
                names.add(chmp.getName());
                response = response + chmp.getName()+"<br>";//codigos html explicados la pela linha 76
                response = response + higher.getStats().getTotalSessionsPlayed()+"<br>";//* linha 76...*
                higher = lower; // resetando o valor de higher para ele ser o menor possivel 
            }
        } catch (RiotApiException e) {
            e.printStackTrace();
        }

        return names;
        //return response; //retornando 3 campeos mais jogados e sua quantidade de partidas
    }

}