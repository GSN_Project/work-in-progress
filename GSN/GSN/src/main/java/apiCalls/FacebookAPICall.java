package apiCalls;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.types.Page;
import com.restfb.types.Post;

@SuppressWarnings("serial")
@WebServlet("/fbapi")
public class FacebookAPICall extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String game = (String)session.getAttribute("game");
		String filme = (String)session.getAttribute("movie");
		
		String acessToken = "EAALZAlmbPPjcBACZAmKvZBjWZBwYAaV4gK4Bqu0ZAVVGHVsdIjlzPc0Dh7chLkZCsWtINWSiaQUy0uCoE9QXMqLK2eb6JZAjyZAgXJEYzgGkTdpZCwh76W2V7lyGA0Yb7xYS7FLAurNiCszH7ZAFZBH61YKO5nhs1NEc3psQUdTuQlYFAZDZD";
		FacebookClient fbClient = new DefaultFacebookClient(acessToken, Version.LATEST);
		Page page = fbClient.fetchObject(filme, Page.class);
		
		
		int currPage = (int)session.getAttribute("fbpage");
		
		Connection<Post> postFeed = fbClient.fetchConnection(page.getId()+"/posts",Post.class,Parameter.with("fields", "object_id,message,full_picture"));
		PrintWriter out = response.getWriter();
		out.println("<html><body>");
		out.println("<table><tr>");
		for(List<Post> postPage : postFeed){
			for(Post aPost : postPage.subList(currPage, currPage+5)){
				out.println("<td>");
				//System.out.println(aPost.getFrom().getName());
				out.println("<h1>");
				out.println("<a href=\"https://www.fb.com/"+aPost.getId() + "\">");
				if(aPost.getMessage() != null){
				out.println("-->"+aPost.getMessage() +"</a>");
				}else{
					out.println("--> No messages included " +"</a>");
				}
				
				out.println("<br>");
				out.println("<img src=\""+ aPost.getFullPicture() + "\" height=\"300\" width=\"300\" />");
				//out.println(aPost.getFullPicture());
				/*out.println("<br>");
				out.println("id do objeto"+aPost.getObjectId());*/
				out.println("</h1>");
				out.println("</td>");
			}
			break;
		}
		out.println("</tr></table>");
		out.println("<form action =\"add\"><input type=\"submit\" value=\"next\" style=\"height:50px;width:100px\" ></form>");
	}
	
}