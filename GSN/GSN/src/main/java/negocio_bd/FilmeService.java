package negocio_bd;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import dao.FilmeDAO;
import dominio.Filme;
import recomendation_film.RecomendacaoFilmeArte;
import recomendation_film.RecomendacaoFilmeEfeitos;
import recomendation_film.RecomendacaoFilmeOriginalidade;
import recomendation_film.RecomendacaoFilmePlot;
import recomendation_film.RecomendacaoFilmeStrategy;
import recomendation_film.RecomendacaoFilmetrilha;

@Stateless
public class FilmeService {
	@Inject
	private FilmeDAO movieDAO;

	public RecomendacaoFilmeStrategy recFilme;
	
	public List<Filme> listarUsers(){
		return movieDAO.listar();
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cadastrarUsuario(Filme game) {
		
		Filme c = movieDAO.buscarFilme(game.getId());
		if (c == null) {
			movieDAO.salvar(game);
		} else {
			movieDAO.atualizar(game);
		}
	}
	
	public List<Filme> recomendarFilme(String login,int sel){
		
		if(sel == 1){
			recFilme = new RecomendacaoFilmeArte();
			return recFilme.recomendarFilme(login, "query");			
		}
		if(sel == 2){
			recFilme = new RecomendacaoFilmeEfeitos();
			return recFilme.recomendarFilme(login, "query");
		}
		if(sel == 3){
			recFilme = new RecomendacaoFilmeOriginalidade();
			return recFilme.recomendarFilme(login, "query");
		}
		if(sel == 4){
			recFilme = new RecomendacaoFilmetrilha();
			return recFilme.recomendarFilme(login, "query");
		}
		if(sel == 5){
			recFilme = new RecomendacaoFilmePlot();
			return recFilme.recomendarFilme(login, "query");
		}
		
		return movieDAO.recomendar(login, "select c from Filme c");
		
	}
}