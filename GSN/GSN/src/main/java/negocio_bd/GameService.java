package negocio_bd;

import java.util.List;

import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import dao.GameDAO;
import dominio.Game;
import recomendation_game.RecomendacaoArte;
import recomendation_game.RecomendacaoGameStrategy;
import recomendation_game.RecomendacaoHistoria;
import recomendation_game.RecomendacaoJogabilidade;
import recomendation_game.RecomendacaoReplay;
import recomendation_game.RecommendGraphic;

@Stateful
public class GameService {
	@Inject
	private GameDAO gameDAO;
	
	public RecomendacaoGameStrategy recGame; 
	
	public List<Game> listarGames(){
		return gameDAO.listar();
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cadastrarUsuario(Game game) {
		Game c = gameDAO.buscar(game.getId());
		if (c == null) {
			gameDAO.salvar(game);
		} else {
			gameDAO.atualizar(game);
		}
	}
	
	public List<Game> recomendarJogo(String login,int sel){
		if(sel == 1){
			recGame = new RecomendacaoArte();
			return recGame.recomendarJogo(login, "query");
		}
		if(sel == 2){
			recGame = new RecomendacaoHistoria();
			return recGame.recomendarJogo(login, "query");
		}
		if(sel == 3){
			recGame = new RecomendacaoJogabilidade();
			return recGame.recomendarJogo(login, "query");
		}
		if(sel == 4){
			recGame = new RecomendacaoReplay();
			return recGame.recomendarJogo(login, "query");
		}
		if(sel == 5){
			recGame = new RecommendGraphic();
			return recGame.recomendarJogo(login, "query");
		}
		return gameDAO.recomendar(login,"select c from Game c");
		
	}
}