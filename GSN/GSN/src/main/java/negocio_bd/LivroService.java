package negocio_bd;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import dao.LivroDAO;
import dominio.Livro;
import recomendation_livro.RecomendacaoLivroAutor;
import recomendation_livro.RecomendacaoLivroCriatividade;
import recomendation_livro.RecomendacaoLivroEscrita;
import recomendation_livro.RecomendacaoLivroIlustracao;
import recomendation_livro.RecomendacaoLivroPlot;
import recomendation_livro.RecomendacaoLivroStrategy;

@Stateless
public class LivroService {
	@Inject
	private LivroDAO bookDAO;

	public RecomendacaoLivroStrategy recLivro;
	
	public List<Livro> listarUsers(){
		return bookDAO.listar();
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cadastrarUsuario(Livro game) {
		
		Livro c = bookDAO.buscarLivro(game.getId());
		if (c == null) {
			bookDAO.salvar(game);
		} else {
			bookDAO.atualizar(game);
		}
	}
	
	public List<Livro> recomendarJogo(String login,int sel){
		
		if(sel == 1){
			recLivro = new RecomendacaoLivroAutor();
			return recLivro.recomendarLivro(login, "query");			
		}
		if(sel == 2){
			recLivro = new RecomendacaoLivroCriatividade();
			return recLivro.recomendarLivro(login, "query");
		}
		if(sel == 3){
			recLivro = new RecomendacaoLivroEscrita();
			return recLivro.recomendarLivro(login, "query");
		}
		if(sel == 4){
			recLivro = new RecomendacaoLivroIlustracao();
			return recLivro.recomendarLivro(login, "query");
		}
		if(sel == 5){
			recLivro = new RecomendacaoLivroPlot();
			return recLivro.recomendarLivro(login, "query");
		}
		
		return bookDAO.recomendar(login,"select c from Livro c");
		
	}
}