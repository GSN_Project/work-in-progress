package controllers;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
//import javax.servlet.ServletContext;

import dominio.Filme;
import negocio_bd.FilmeService;

@ManagedBean
@SessionScoped
public class FilmeMB extends SelectorMB{
	private Filme movie;
	
	@EJB
	private FilmeService movieService;
	
	private List<Filme> listaMovies;
	
	private List<Filme> secondMovieList;
	
	public FilmeMB() {
		movie = new Filme();
	}
	

	public List<Filme> getListaMovies() {
		setListaMovies(movieService.listarUsers());
		return listaMovies;
	}


	public void setListaMovies(List<Filme> listaMovies) {
		this.listaMovies = listaMovies;
	}
	
	public Filme getMovie() {
		return movie;
	}


	public void setMovie(Filme movie) {
		this.movie = movie;
	}


	public List<Filme> getSecondMovieList() {
		FacesContext context = FacesContext.getCurrentInstance();
		String login = (String)context.getExternalContext().getSessionMap().get("login");		
		
		int selector = (Integer)context.getExternalContext().getSessionMap().get("selector");
		
		setSecondMovieList( movieService.recomendarFilme(login, selector));		
		return secondMovieList;
	}


	public void setSecondMovieList(List<Filme> secondMovieList) {
		this.secondMovieList = secondMovieList;
	}


	public String cadastrar() {
		FacesContext context = FacesContext.getCurrentInstance();
		String login = (String)context.getExternalContext().getSessionMap().get("login");
		movie.setLogin(login);
		
		movieService.cadastrarUsuario(movie);
		movie = new Filme();
		return "filmerate.jsf";
	}


}