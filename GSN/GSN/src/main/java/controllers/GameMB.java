package controllers;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import dominio.Game;
import negocio_bd.GameService;

@ManagedBean
@SessionScoped
public class GameMB extends SelectorMB{
	private Game game;
	
	@EJB
	private GameService registerService;
	
	private List<Game> listaGames;
	
	private List<Game> secondGameList;
	
	public GameMB() {
		game = new Game();
	}
	

	public List<Game> getListaGames() {
		setListaGames( registerService.listarGames() );		
		
		/*ArrayList<String> gamesNrates = new ArrayList<String>();
		
		int media1=0;
		int media2=0;
		int media3=0;
		int media4=0;
		int media5=0;
		
		for(Game g : listaGames){
			media = media + g.getArte();
		}
		
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().put("media", media);*/
		
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().put("listaGames", listaGames);
		
		return listaGames;
	}


	public void setListaGames(List<Game> listaGames) {
		this.listaGames = listaGames;
	}


	public Game getGame() {
		return game;
	}


	public void setGame(Game game) {
		this.game = game;
	}
	
	/*public List<Game> recomendacao(){
		FacesContext context = FacesContext.getCurrentInstance();
		String login = (String)context.getExternalContext().getSessionMap().get("login");
		setListaGames( registerService.recomendarJogo(login) );
		return listaGames;
	}*/


	public List<Game> getSecondGameList() {
		
		
		FacesContext context = FacesContext.getCurrentInstance();
		String login = (String)context.getExternalContext().getSessionMap().get("login");		
		
		int selector = (Integer)context.getExternalContext().getSessionMap().get("selector");
		
		setSecondGameList( registerService.recomendarJogo(login,selector));
		return secondGameList;
	}


	public void setSecondGameList(List<Game> secondGameList) {
		this.secondGameList = secondGameList;
	}


	public String cadastrar() {
		FacesContext context = FacesContext.getCurrentInstance();
		String login = (String)context.getExternalContext().getSessionMap().get("login");
		game.setLogin(login);
		
		
		registerService.cadastrarUsuario(game);
		game = new Game();
		return "gamerate.jsf";
	}
}