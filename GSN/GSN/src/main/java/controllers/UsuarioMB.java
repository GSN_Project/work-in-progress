package controllers;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import dominio.Usuario;
import negocio_bd.LoginService;

@ManagedBean
@SessionScoped
public class UsuarioMB extends SelectorMB{
	private Usuario usuario;
	
	@EJB
	private LoginService loginService;
	
	private List<Usuario> listaUsers;
	

	public UsuarioMB() {
		usuario = new Usuario();
	}
	

	
	public List<Usuario> getListaUsers() {
		setListaUsers(loginService.listarUsers());
		return listaUsers;
	}



	public void setListaUsers(List<Usuario> listaUsers) {
		this.listaUsers = listaUsers;
	}



	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@SuppressWarnings("unchecked")
	public String login() {
		int res = loginService.login(usuario.getLogin(), usuario.getSenha());
		if (res == 1) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getSessionMap().put("login", usuario.getLogin());
			context.getExternalContext().getSessionMap().put("senha", usuario.getSenha());
			context.getExternalContext().getSessionMap().put("fbpage", 0);
			
			
			
			ArrayList<Usuario> users = new ArrayList<Usuario>();
			ServletContext srvletcontext = (ServletContext) FacesContext.
					getCurrentInstance().getExternalContext().getContext();
						
			if(srvletcontext.getAttribute("connected") != null ){
				users = (ArrayList<Usuario>) srvletcontext.getAttribute("connected");
			}
			users.add(usuario);
			srvletcontext.setAttribute("connected", users);
			
			context.getExternalContext().getSessionMap().put("game","leagueoflegends");
			context.getExternalContext().getSessionMap().put("selector",0);
			//return "localhost:1880/tweetlist";
			return "/userpage.jsf";
		} else if (res == 0) {
			FacesMessage msg = new FacesMessage("Usuario e/ou senha incorretos");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage("", msg);
			return null;
		} else {
			FacesMessage msg = new FacesMessage("Usuario nao encontrado");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage("", msg);
			return null;
		}
	}
	
	public String cadastrar() {
		loginService.cadastrarUsuario(usuario);
		usuario = new Usuario();
		return "page.jsf";
	}
	
	public void remover(){
		loginService.removerContato(usuario.getLogin());
		//return "lista.jsf";
	}

}