package controllers;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
//import javax.servlet.ServletContext;

import dominio.Livro;
import negocio_bd.LivroService;

@ManagedBean
@SessionScoped
public class LivroMB extends SelectorMB{
	private Livro livro;
	
	@EJB
	private LivroService livroService;
	
	private List<Livro> listaLivros;
	
	private List<Livro> secondListaLivro;
	

	public LivroMB() {
		livro = new Livro();
	}


	
	public Livro getLivro() {
		return livro;
	}



	public void setLivro(Livro livro) {
		this.livro = livro;
	}



	public List<Livro> getListaLivros() {
		setListaLivros(livroService.listarUsers());
		return listaLivros;
	}



	public void setListaLivros(List<Livro> listaLivros) {
		this.listaLivros = listaLivros;
	}



	public List<Livro> getSecondListaLivro() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		String login = (String)context.getExternalContext().getSessionMap().get("login");		
		
		int selector = (Integer)context.getExternalContext().getSessionMap().get("selector");
		setSecondListaLivro( livroService.recomendarJogo(login, selector) );
		
		
		return secondListaLivro;
	}



	public void setSecondListaLivro(List<Livro> secondListaLivro) {
		this.secondListaLivro = secondListaLivro;
	}



	public String cadastrar() {
		FacesContext context = FacesContext.getCurrentInstance();
		String login = (String)context.getExternalContext().getSessionMap().get("login");
		livro.setLogin(login);
		
		livroService.cadastrarUsuario(livro);
		livro = new Livro();
		return "livrorate.jsf";
	}


}