/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Connector;

//import com.mysql.jdbc.Connection;
/*import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import sun.util.logging.PlatformLogger;
*/
//import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
/**
 *
 * @author the_p_000
 */
public class Main {//Essa classe ao contrario do nome nao eh a principal ¬¬, ela serve para conectar o java ao JDBC
    private static final String DRIVER="com.mysql.jdbc.Driver";//driver mysql
    private static final String URL="jdbc:mysql://localhost:3306/wildflysqma";//ATENCAO K7,ISSO AKI EH O NOME DA BASE DE DADOS NO *MEU* COMPUTADOR, PORTANTO CASO TENHA Q FUNCIONAR EM OUTRO PC SERA OUTRO NOME
    private static final String USER="root";
    private static final String PASS="paulo1";
    
    public static Connection getConection(){
        try {
            Class.forName(DRIVER);//conectando o driver mysql
            
            return DriverManager.getConnection(URL, USER, PASS);
        } catch (ClassNotFoundException | SQLException ex) {
            throw new RuntimeException("Erro na conexão"+ex);
        }
    }
    public static void closeConnection(Connection con){
        try{
            if(con != null){
                con.close();//fechando driver mysql
            }
        }catch(SQLException ex){
            throw new RuntimeException("Erro ao fechar conexão",ex);
        }
    }
    
    public static void closeConnection(Connection con, PreparedStatement stat){
        closeConnection(con);
        try{
            if(stat != null){
                con.close();//fechando driver mysql
            }
        }catch(SQLException ex){
            throw new RuntimeException("Erro ao fechar conexão",ex);
        }
    }
    public static void closeConnection(Connection con, PreparedStatement stat,ResultSet rs){
        closeConnection(con,stat);
        try{
            if(rs != null){
                con.close();//fechando driver mysql
            }
        }catch(SQLException ex){
            throw new RuntimeException("Erro ao fechar conexão",ex);
        }
    }
}
