/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelBean.User;
import Connector.Main;

/**
 *
 * @author the_p_000
 */
public class UserDAO {//classe sera utilizada para checar o login de um usuario com o banco de dados
        public boolean checkLogin(String login,String senha){
        
        Connection con = Main.getConection(); //conexao principal com o bd sendo instanciada
        PreparedStatement stmt = null;//query sql
        ResultSet rs = null;
        boolean check = false;
        
        try {
            stmt = con.prepareStatement("SELECT * FROM usuarios WHERE login = ? and senha = ?");//SQL
            stmt.setString(1, login);//parametro 1 ( 1a ? ) (INTERROGACAO DO SQL ACIMA)
            stmt.setString(2, senha);//parametro 2 ( 2a ? )
            
            rs = stmt.executeQuery(); //execucao do comando SQL
            
            if(rs.next()){             
                check = true; //nao lembro pra que serve. . . tem a ver com confirmacao da query sql
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            Main.closeConnection(con,stmt,rs);
        }
        return check;
    }
}
