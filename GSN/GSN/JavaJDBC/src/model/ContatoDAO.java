/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelBean.Contato;
import Connector.Main;

/**
 *
 * @author the_p_000
 */
public class ContatoDAO {// Essa classe aki nao serve meio que pra nada por enquanto, mas basicamente ele mostra o que esta cadastrado no bd
    /**
     * 
     * @param c = Classe a ser jogada no bd que esta sendo criado
     */
    public void create(Contato c){
        Connection con = Main.getConection();//instanciando a conexao com o bd
        PreparedStatement stmt = null;//query sql
        
        try {
            stmt = con.prepareStatement("INSERT INTO contatos(id,email,nome,telefone)VALUES(?,?,?,?)");//sql
            stmt.setInt(1, c.getId());//primeiro parametro -> 1a ? ( INTERROGACAO DO SQL ACIMA )
            stmt.setString(2, c.getEmail());//segundo parametro -> 2a ?
            stmt.setString(3, c.getNome());//terceiro parametro -> 3a ?
            stmt.setString(4, c.getTelefone());//quarto parametro -> 4a ?
            
            stmt.executeUpdate();//atualizando o bd
            
            JOptionPane.showMessageDialog(null, "Salvo com sucesso");
        } catch (SQLException ex) {
            Logger.getLogger(ContatoDAO.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Erro ao salvar");
        }finally{
            Main.closeConnection(con,stmt);
        }
    }
    /**
     * 
     * @return retorna os elementos que irao ser recuperados do bd
     */
    public List<Contato> read(){
        
        Connection con = Main.getConection();//instanciando o bd
        PreparedStatement stmt = null;//sql query
        ResultSet rs = null;
        List<Contato> contatos = new ArrayList<>();//lista a ser devolvida futuramente
        try {
            stmt = con.prepareStatement("SELECT * FROM contatos");//sql query
            rs = stmt.executeQuery();//executando sql
            
            
            
            while(rs.next()){
                Contato c = new Contato();
                
                c.setId(rs.getInt("id"));
                c.setEmail(rs.getString("email"));
                c.setNome(rs.getString("nome"));
                c.setTelefone(rs.getString("telefone"));
                contatos.add(c);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ContatoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            Main.closeConnection(con,stmt,rs);
        }
        return contatos;
    }
    /**
     * 
     * @param c = contato a ser modificado no bd
     */
    public void update(Contato c){
        Connection con = Main.getConection();//instanciando conexao ao bd
        PreparedStatement stmt = null;//sql query
        
        try {
            stmt = con.prepareStatement("UPDATE contatos SET email = ?, nome = ?,telefone = ? WHERE id= ?");//sql
            
            stmt.setString(1, c.getEmail());//1a ? primeiro parametro
            stmt.setString(2, c.getNome());//2a ? segundo parametro
            stmt.setString(3, c.getTelefone());//3a ? terceiro parametro
            stmt.setInt(4, c.getId());//4a ? quarto parametro
            //stmt.setInt
            
            stmt.executeUpdate();
            
            JOptionPane.showMessageDialog(null, "Atualizado com sucesso");
        } catch (SQLException ex) {
            Logger.getLogger(ContatoDAO.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Erro ao atualizar");
        }finally{
            Main.closeConnection(con,stmt);
        }
    }
    /**
     * 
     * @param c = contato a ser deletado do bd 
     */
    public void delete(Contato c){
        Connection con = Main.getConection();//instanciando bd
        PreparedStatement stmt = null;//sql query
        
        try {
            stmt = con.prepareStatement("DELETE FROM contatos WHERE id= ?");//sql
            stmt.setInt(1, c.getId());
            
            stmt.executeUpdate();
            
            JOptionPane.showMessageDialog(null, "Excluido com sucesso");
        } catch (SQLException ex) {
            Logger.getLogger(ContatoDAO.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Erro ao excluir");
        }finally{
            Main.closeConnection(con,stmt);
        }
    }
}
